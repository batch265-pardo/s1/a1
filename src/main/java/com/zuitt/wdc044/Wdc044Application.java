package com.zuitt.wdc044;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

//"@SpringBootApplication" this is called as "Annotations" mark.
	//Annotations are used to provide supplemental information about the program
	//These are used to manage and configure the behavior of the framework.
@SpringBootApplication
//Tells the spring boot that this will handle endpoints for web request.
@RestController
public class Wdc044Application {

	public static void main(String[] args) {
		//this method starts the whole Spring framework.
		SpringApplication.run(Wdc044Application.class, args);
	}

	//This is used for mapping HTTP GET request.
	@GetMapping("/hello")
	//"@RequestParam" is used to extract query parameters, form parameters and even files from the request
		//name = john; Hello John
		// To append the URL with a name parameter we do the following:
			//http://localhost:8080/hello?name=John
			//"?" means the start of the parameters followed by the "key-value" pairs
	public String hello(@RequestParam(value = "name", defaultValue = "World") String name){
		return String.format("Hello %s", name);
	}
	@GetMapping("/greetings")
	public String greetings(@RequestParam(value = "greet", defaultValue = "World") String greet){
		return String.format("Good evening, %s! Welcome to Batch 265", greet);
	}

	@GetMapping("/contactInfo")
	public String contactInfo(@RequestParam(value="name", defaultValue = "User") String name, @RequestParam(value="email", defaultValue = "user@mail.com") String email){
		return String.format("Hello %s! Your email is %s", name, email);
	}

	@GetMapping("/hi")
	public String user(@RequestParam(value="user", defaultValue = "user") String user){
		return String.format("hi %s", user);
	}

	@GetMapping("/nameAge")
	public String user(@RequestParam(value="user", defaultValue = "user") String user, @RequestParam(value="age", defaultValue = "0") int age){
		return String.format("Hello %s! Your age is %d.", user, age);
	}

}
