package com.zuitt.wdc044.exceptions;

//This will hold an exception(error) message during the registrations
public class UserException extends Exception {
    public UserException(String message){
        super(message);
    }
}
