package com.zuitt.wdc044.controllers;

import com.zuitt.wdc044.models.Post;
import com.zuitt.wdc044.repositories.PostRepository;
import com.zuitt.wdc044.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
public class PostController {
    @Autowired
    PostService postService;

    // create a new post
    @RequestMapping(value = "/posts", method = RequestMethod.POST)
    public ResponseEntity<Object> createPost(@RequestHeader(value = "Authorization") String stringToken, @RequestBody Post post){

        postService.createPost(stringToken, post);
        return new ResponseEntity<>("Post created successfully", HttpStatus.CREATED);
    }
    //Activity
    // public ResponseEntity<Object> getPosts(){ return new ResponseEntity<>(postService.getPosts(), HttpStatus.OK)}
    // public Iterable<Post> getPosts(){ return postService.getPosts() }
    @RequestMapping(value = "/posts", method = RequestMethod.GET)
    public ResponseEntity<Object> getPosts(){
        Iterable<Post> posts = postService.getPosts();
        if(posts.iterator().hasNext()){
            return new ResponseEntity<>(posts, HttpStatus.OK);
        }else{
            return new ResponseEntity<>("No Post", HttpStatus.OK);
        }
    }

    //Edit post
    @RequestMapping(value = "/posts/{postId}", method = RequestMethod.PUT)
    //@PathVariable is used for data passed in URI
    //@PathVariable is used to retrieve exact record
    public ResponseEntity<Object> updatePost(@PathVariable Long postId, @RequestHeader(value = "Authorization") String stringToken, @RequestBody Post post){
        return postService.updatePost(postId, stringToken, post);
    }

    //Delete post
    @RequestMapping(value = "/posts/{postId}", method = RequestMethod.DELETE)
    //@PathVariable is used for data passed in URI
    //@PathVariable is used to retrieve exact record
    public ResponseEntity<Object> deletePost(@PathVariable Long postId, @RequestHeader(value = "Authorization") String stringToken){
        return postService.deletePost(postId, stringToken);
    }

    @RequestMapping(value = "/myPosts", method = RequestMethod.GET)
    public ResponseEntity<Object> getUserPosts(@RequestHeader(value = "Authorization") String stringToken){

        return new ResponseEntity<>(postService.getUserPosts(stringToken), HttpStatus.OK);
    }

}